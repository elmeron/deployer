# node-deployer

Update script on GitLab web hook.


## Example

```javascript
// parent script
var deployer = require('deployer').Parent;

deployer.deploy('path/to/child');


// child script
var deployer = require('deployer').Child;

deployer.hello().then(function() {
  someService.on('update', deployer.update());
});
```


## Parent
