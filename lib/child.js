var Promise = require('bluebird');
var path = require('path');
var git = require('simple-git')(path.join(__dirname, '..'));
var RESTART_CODE = require('./util').RESTART_CODE;

exports.hello = function(message) {
  return new Promise(function(resolve, reject) {
    try {
      process.send('ping');
      process.on('message', function(message) {
        if (message === 'pong') {
          return resolve();
        }
      });
    }
    catch (err) {
      return reject(err);
    }
  });
};

exports.update = function() {
  return new Promise(function(resolve, reject) {
    git
      .pull(function(err, update) {
        if (err) {
          return reject(err);
        }
        if (update && !update.summary.changes) {
          return reject(new Error('nothing to update'));
        }
      })
      .tags(function(err, tags) {
        if (err) {
          reject(err);
        }

        git.checkout('-b' + tags.latest, function(err, data) {
          if (err) {
            reject(err);
          }
          process.exit(RESTART_CODE);
        });
      });
  });
};
