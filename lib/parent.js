var fork = require('child_process').fork;
var EventEmitter = require('events').EventEmitter;
var util = require('util');
var RESTART_CODE = require('./util').RESTART_CODE;

var CHILD_EXIT = 'CHILD_EXIT';
var CHILD_START = 'CHILD_START';

var child;
var reason;

/**
 * Deployer Parent.
 */
function Parent() {
  EventEmitter.call(this);
}

Parent.prototype.deploy = function(path, options) {
  if (child && child.connected) {
    throw new Error('can\'t deploy, child already running!');
  }

  var that = this;

  child = fork(path, options);

  child.on('message', function(message) {
    if (message === 'ping') {
      that.emit(CHILD_START);
      child.send('pong');
    }
  });

  child.on('error', function(err) {
    reason = err;
  });

  child.on('exit', function(code) {
    that.emit(CHILD_EXIT, { code, reason });
    child.removeAllListeners();
    if (code === RESTART_CODE) {
      that.deploy(path, options);
    }
  });
};

Parent.prototype.kill = function() {
  if (!child || !child.connected) {
    throw new Error('no child alive to kill');
  }

  child.kill();
};

util.inherits(Parent, EventEmitter);
module.exports = new Parent();
