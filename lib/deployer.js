/**
 * Deployer
 */

var Parent = require('./parent');
var Child = require('./child');

module.exports.Parent = Parent;
module.exports.Child = Child;
