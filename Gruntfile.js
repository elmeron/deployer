module.exports = function(grunt) {
  require('grunt-task-loader')(grunt);

  grunt.initConfig({
    nodeunit: {
      all: ['test/*-test.js'],
      options: {
        reporter: 'default'
      }
    }
  });

  grunt.registerTask('test', ['nodeunit']);
};
