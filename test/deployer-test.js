/**
 * Deployer test
 */

var path = require('path');
var deployer = require('../lib/deployer').Parent;
var git = require('simple-git')(path.join(__dirname, '..'));
var scripts = path.join(__dirname, 'scripts');

exports.testBadPath = function(test) {
  test.expect(1);
  deployer.deploy('cool/path', { silent: true });

  deployer.once('CHILD_EXIT', function(data) {
    test.equal(data.code, 1);
    test.done();
  });

  deployer.once('CHILD_START', function() {
    test.ok(0);
  });
};

exports.testSimpleChild = function(test) {
  test.expect(2);
  deployer.deploy(path.join(scripts, 'simple-child.js'));

  deployer.once('CHILD_EXIT', function(data) {
    test.equal(data.code, 0);
    test.done();
  });

  deployer.once('CHILD_START', function() {
    test.ok(1);
  });
};

exports.testRestartChild = function(test) {
  var starts = 0;

  test.expect(4);
  deployer.deploy(path.join(scripts, 'restart-child.js'));

  deployer.on('CHILD_START', function() {
    test.ok(1);
    if (starts++) {
      deployer.kill();
      git.checkout('master')
         .then(function() {
           test.done();
         });
    }
  });

  deployer.on('CHILD_EXIT', function(data) {
    test.ok(1);
  });
};
