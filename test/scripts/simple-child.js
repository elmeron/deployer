/**
 * Simple Child
 */


var deployer = require('../../lib/deployer').Child;

deployer
  .hello()
  .then(function() {
    process.exit();
  })
  .catch(function(err) {
    console.error(err);
  });
