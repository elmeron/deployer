/**
 * Restart Child
 */

var deployer = require('../../lib/deployer').Child;

deployer.hello().then(function() {
  deployer.update().catch(function(err) {
    console.error(err);
  });
});
